﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server
{
    public interface IUserDAL
    {
        List<User> GetAll();
        User getByName(string name);
        void Add(User user);
        void Update(User user);
        void Delete(string userName);
    }
}
