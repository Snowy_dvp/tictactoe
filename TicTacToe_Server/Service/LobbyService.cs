﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace TicTacToe_Server.Service
{
    public class LobbyService : ILobbyService
    {
        static IList<GameService> games = new List<GameService>();

        public IList<User> usersWaiting { get; set; }
        private Dictionary<string, string> invitations;
        private IUserDAL userDal;
        private readonly IHubContext<ServerMessage> hubContext;

        public LobbyService([FromServices] IUserDAL userDal, IHubContext<ServerMessage> hubContext)
        {
            invitations = new Dictionary<string, string>();
            this.userDal = userDal;
            this.usersWaiting = new List<User>();
            this.hubContext = hubContext;
        }

        public bool ConnectPlayer(string userName)
        {
            User user = userDal.getByName(userName);
            if (usersWaiting.SingleOrDefault(u => u.Name == userName) == null) //TODO Fix fake user name
            {
                usersWaiting.Add(user);
            }
            return true;
        }

        public void RemovePlayer(string userName)
        {
            User user = userDal.getByName(userName);
            if (usersWaiting.SingleOrDefault(u => u.Name == userName) != null)
            {
                foreach (var userWait in usersWaiting)
                {
                    if (userWait.Name == userName)
                    {
                        usersWaiting.Remove(userWait);
                        break;
                    }
                }
            }
        }

        public async void InvitePlayer(string userNameInviter, string userNameInvited)
        {
            User userInviter = userDal.getByName(userNameInviter);
            User userInvited = userDal.getByName(userNameInvited);
            if (!invitations.ContainsKey(userInviter.Name))
            {
                invitations.Add(userInviter.Name, userInvited.Name);
                await hubContext.Clients.All.SendAsync("Invite", userInvited.Name, userInviter.Name);

            }

        }

        public async void AcceptInvitation(string userNameInvited, string userNameInviter)
        {
            User user = userDal.getByName(userNameInviter);
            User userInvited = userDal.getByName(userNameInvited);
            if (invitations.ContainsValue(userInvited.Name) && invitations.GetValueOrDefault(userNameInviter) == userNameInvited)
            {
                //Création et ajout d'une nouvelle game
                GameService game = new GameService(user, userInvited, hubContext);
                games.Add(game);
                RemoveInvitation(user);
                //TODO notify inviter user
                await hubContext.Clients.All.SendAsync("StartGame", userNameInvited, userNameInviter);
            }
        }

        public void DeclineInvitation(string userNameDecliner, string userName)
        {
            User user = userDal.getByName(userName);
            User userDecliner = userDal.getByName(userNameDecliner);
            if (invitations.GetValueOrDefault(userName) == userNameDecliner)
            {
                RemoveInvitation(user);
                //TODO notify inviter user
            }
        }

        public void CancelInvitation(string userNameCanceler, string userName)
        {
            User user = userDal.getByName(userName);
            User userCanceler = userDal.getByName(userNameCanceler);
            if (invitations.ContainsValue(userNameCanceler) && invitations.GetValueOrDefault(userName) == userNameCanceler)
            {
                RemoveInvitation(user);
                //TODO request client
            }
        }

        public void RemoveInvitation(User user)
        {
            invitations.Remove(user.Name);
        }
    }
}
