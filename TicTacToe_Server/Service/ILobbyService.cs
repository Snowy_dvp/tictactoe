﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server.Service
{
    public interface ILobbyService
    {
        IList<User> usersWaiting { get; set; }
        bool ConnectPlayer (string userName);
        void RemovePlayer (string userName);
        void InvitePlayer (string userName, string userIdInvited);
        void AcceptInvitation(string userNameInvited, string userName);
        void DeclineInvitation(string userNameDecliner, string userName);
        void CancelInvitation(string userNamesCanceler, string userName);
    }
}
