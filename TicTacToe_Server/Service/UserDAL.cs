﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server
{
    public class UserDAL : IUserDAL
    {
        public List<User> GetAll()
        {
            using (var context = new MorpionContext())
            {
                return context.Users.ToList();
            }
        }

        public User getByName(string name)
        {
            using (var context = new MorpionContext())
            {
                User user = context.Users.SingleOrDefault(u => u.Name == name);
                return user;
            }
        }

        public void Add(User user)
        {
            using (var context = new MorpionContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }
        public void Update(User user)
        {
            using (var context = new MorpionContext())
            {
                context.Users.Update(user);
                context.SaveChanges();
            }
        }

        public void Delete(string userName)
        {
            using (var context = new MorpionContext())
            {
                User user = context.Users.SingleOrDefault(u => u.Name == userName);
                context.Users.Remove(user);
                context.SaveChanges();
            }
        }
    }
}
