﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicTacToe_Server.Migrations
{
    public partial class Morpion1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Name = table.Column<string>(maxLength: 25, nullable: false),
                    password = table.Column<string>(nullable: false),
                    Win = table.Column<double>(nullable: false),
                    Lose = table.Column<double>(nullable: false),
                    Percentage = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Name);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
