﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server
{
    public class User
    {
        public User(string name)
        {
            this.Name = name;
        }

        public User (string name, string password)
        {
            this.Name = name;
            this.password = password;
        }

 //       [Key]
//        public int Id { get; set; }
 //       [Required]
        [Key]
        [MaxLength(25)]
        public string Name { get; set; }

        [Required]
        public string password { get; set; }
        public double Win { get; set; }
        public double Lose { get; set; }
        public double Percentage { get; set; }
    }
}
