﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server.Model
{
    public class JoinRequest
    {
        public string userName { get; set; }
    }
}
