﻿using System.Threading.Tasks;

namespace TicTacToe_Server
{
    public interface IClientMessage
    {
        Task Invite(string userNameInvited, string userNameInviter);
    }
}