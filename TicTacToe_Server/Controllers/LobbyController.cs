﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using TicTacToe_Server.Model;
using TicTacToe_Server.Service;

namespace TicTacToe_Server.Controllers
{
    [Route("lobby/")]
    [ApiController]
    public class LobbyController : ControllerBase
    {
        [HttpPost]
        [Route("join")]
        public ActionResult JoinLobby([FromServices] ILobbyService lobby, [FromServices] IUserDAL userDAL, [FromBody] LogonRequest logon)
        {
            User user = userDAL.getByName(logon.userName);
            if (user == null || logon.password != user.password)
                return Forbid();
            lobby.ConnectPlayer(logon.userName);
            return NoContent();
        }

        [HttpPost]
        [Route("leave")]
        public void QuitLobby([FromServices] ILobbyService lobby, [FromBody] JoinRequest request)
        {
            lobby.RemovePlayer(request.userName);
        }

        [HttpGet]
        [Route("users")]
        public string getWaitingUsers([FromServices] ILobbyService lobby)
        {
            return JsonConvert.SerializeObject(lobby.usersWaiting);

        }

        [HttpPost]
        [Route("invite")]
        public void InviteUser([FromServices] ILobbyService lobby, [FromBody] InvitationRequest invitation)
        {
            lobby.InvitePlayer(invitation.UserName1, invitation.UserName2);
            //TODO invited user doesn't exist
        }

        [HttpPost]
        [Route("accept")]
        public void AcceptInvitation([FromServices] ILobbyService lobby, [FromBody] InvitationRequest invitation)
        {
            lobby.AcceptInvitation(invitation.UserName1, invitation.UserName2);
        }

        [HttpPost]
        [Route("decline")]
        public void DeclineInvitation([FromServices] ILobbyService lobby, [FromBody] InvitationRequest invitation)
        {
            lobby.DeclineInvitation(invitation.UserName1, invitation.UserName2);
        }

        [HttpPost]
        [Route("cancel")]
        public void CancelInvitation([FromServices] ILobbyService lobby, [FromBody] InvitationRequest invitation)
        {
            lobby.CancelInvitation(invitation.UserName1, invitation.UserName2);
        }
    }
}