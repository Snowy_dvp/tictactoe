﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TicTacToe_Server;
using TicTacToe_Server.Model;
using Entities;

namespace TicTacToe_Server.Controllers
{
    [Route("login")]
    public class LoginController : Controller
    {

        public LoginController()
        {
        }

        
        public IActionResult SignupSuccess()
        {
            return View();
            //TODO create success page
        }

        // GET: Logins/Create
        [HttpGet]
        [Route("signup")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Logins/Create
        [HttpPost]
        [Route("signup")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("name,password,confirmPassword")] Login login, [FromServices] IUserDAL userDAL)
        {
            if (ModelState.IsValid && login.password == login.confirmPassword)
            {
                if (userDAL.getByName(login.name) == null)
                {
                    User user = new User(login.name, login.password);
                    userDAL.Add(user);
                    return RedirectToAction(nameof(SignupSuccess));
                }
                //return RedirectToAction(nameof(UserExist));
                return null;
            }
            //return RedirectToAction(nameof(PasswordError));
            return null;
        }
    }
}
