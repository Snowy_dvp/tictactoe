﻿using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server
{
    public class MorpionContext : DbContext
    {
        public MorpionContext(DbContextOptions<MorpionContext> options)
        {

        }

        public MorpionContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Morpion;Trusted_Connection=True;ConnectRetryCount=0");
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //   modelBuilder.Entity<User>().HasData(new Blog { Id = 1, Url = "http://sample.com" });
        //    modelBuilder.Entity<Post>().HasData(new Post { BlogId = 1, Id = 1, Title = "First post", Content = "Test 1" });
        //}

        public DbSet<User> Users { get; set; }

    }
}
