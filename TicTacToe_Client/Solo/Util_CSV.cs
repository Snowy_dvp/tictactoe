﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicTacToe_DisizAndSnowy
{
    class Util_CSV
    {
        #region Public
        public static string fileName = "FileStats";
        #endregion

        public static IEnumerable<User> ReadCSV()
        {
            string[] lines = File.ReadAllLines(System.IO.Path.ChangeExtension(fileName, ".csv"));
            List<User> listReturn = new List<User>();

            foreach (string line in lines)
            {
                string[] data = line.Split(';');
                if (data[0] != "")
                    listReturn.Add(new User(data[0], int.Parse(data[1]), int.Parse((data[2]))));
            }
            return listReturn;
        }


        public static void WriteCsv(string nameW, string nameL)
        {
            List<User> users = ReadCSV().ToList();

            if (users.Any(user => user.Name == nameW))
                users.SingleOrDefault(user => user.Name == nameW).Win++;
            else
                users.Add(new User(nameW, 1, 0));

            if (users.Any(user => user.Name == nameL))
                users.SingleOrDefault(user => user.Name == nameL).Lose++;
            else
                users.Add(new User(nameL, 0, 1));

            using(var v = new StreamWriter(fileName + ".csv"))
            {
                for (int i = 0; i < users.Count; i++)
                    v.WriteLine(string.Format("{0};{1};{2}", users[i].Name, users[i].Win, users[i].Lose));
            }
        }


        public static void EmptyCSV()
        {
            using (var v = new StreamWriter(fileName + ".csv")){}
        }
    }
}
