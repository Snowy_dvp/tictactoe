﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_DisizAndSnowy
{
    class ModelUserList
    {
        public string Name { get; set; }
        public double Win { get; set; }
        public double Lose { get; set; }
        public double Percentage { get; set; }
    }
}
