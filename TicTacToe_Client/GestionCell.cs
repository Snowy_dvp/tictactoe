﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_DisizAndSnowy
{
    #region Enumerate CellType
    public enum CellType
    {
        //-- Libre
        Free,
        
        //-- Joueur 1
        Cross,

        //-- Joueur 2
        Circle,
    }
    #endregion
}
