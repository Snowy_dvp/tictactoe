﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_DisizAndSnowy
{
    class Multi
    {
        #region POST
        private static HttpClient client = new HttpClient();
        private static bool initiate = false;

        //-- Rejoindre le lobby
        public static async Task<bool> JoinLobbyAsync(string nameP1, string passP1)
        {
            //TODO /!\ A MODIFIER /!\
            if (!initiate)
            {
                client.BaseAddress = new Uri("http://localhost:60584");
                initiate = !initiate;
            }

            var nameObj = new { UserName = nameP1, password = passP1 };
            var json = JsonConvert.SerializeObject(nameObj);
            var result = await client.PostAsync("/lobby/join", new StringContent(json, Encoding.UTF8, "application/json"));
            return result.IsSuccessStatusCode;
        }


        //-- Quitter le lobby
        public static Task LeaveLobbyAsync(string nameP1)
        {
            return Task.Run(() => LeaveLobby(nameP1));
        }
        public static void LeaveLobby(string nameP1)
        {
            var nameObj = new { UserName = nameP1 };
            var json = JsonConvert.SerializeObject(nameObj);
            var result = client.PostAsync("/lobby/leave", new StringContent(json, Encoding.UTF8, "application/json"));
        }

        public static void AcceptInvit(string localUserName, string distantUserName)
        {
            var userPair = new { UserName1 = localUserName, UserName2 = distantUserName };
            var json = JsonConvert.SerializeObject(userPair);
            var result = client.PostAsync("/lobby/accept", new StringContent(json, Encoding.UTF8, "application/json"));
        }


        //-- Decliner une invitation
        public static Task DeclineInvitAsync(string nameP2, string nameP1)
        {
            return Task.Run(() => DeclineInvit(nameP1, nameP2));
        }
        public static void DeclineInvit(string nameP1, string nameP2)
        {
            var nameObj = new { UserName1 = nameP1, UserName2 = nameP2 };
            var json = JsonConvert.SerializeObject(nameObj);
            var result = client.PostAsync("/lobby/cancel", new StringContent(json, Encoding.UTF8, "application/json"));
        }


        //-- Envoie une invitation
        public static Task InvitAsync(string nameP1, string nameP2)
        {
            return Task.Run(() => Invit(nameP1, nameP2));
        }
        public static void Invit(string nameP1, string nameP2)
        {
            var nameObj = new { UserName1 = nameP1, UserName2 = nameP2 };
            var json = JsonConvert.SerializeObject(nameObj);
            var result = client.PostAsync("/lobby/invite", new StringContent(json, Encoding.UTF8, "application/json"));
        }


        //-- Annule une invitation
        public static Task CancelInvitAsync(string nameP2, string nameP1)
        {
            return Task.Run(() => CancelInvit(nameP2, nameP1));
        }
        public static void CancelInvit(string nameP2, string nameP1)
        {
            var nameObj = new { UserName1 = nameP2, UserName2 = nameP1 };
            var json = JsonConvert.SerializeObject(nameObj);
            var result = client.PostAsync("/lobby/cancel", new StringContent(json, Encoding.UTF8, "application/json"));
        }


        //-- Envoie victoire/défaite
        public static Task<User> SendStatsAsync()
        {
            return Task.Run(() => SendStats());
        }
        public static User SendStats()
        {
            User tempo = new User();
            return tempo;
        }
        #endregion


        #region GET
        //-- Lancement de la partie
        public static Task<bool> GameLaunchAsync()
        {
            return Task.Run(() => GameLaunch());
        }
        public static bool GameLaunch()
        {
            return false; //-- Retourne si l'on commence
        }

        
        //-- Rafraichis liste joueurs
        public static async Task<ModelUserList[]> RefreshAsync()
        {
            var nameObj = new { UserName = "ok" };
            var json = JsonConvert.SerializeObject(nameObj);
            HttpResponseMessage result = await client.GetAsync("/lobby/users");
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            ModelUserList[] listObjUser = JsonConvert.DeserializeObject<ModelUserList[]>(responseBody);
            return listObjUser;
        }


        //-- Récupère les stats
        public static Task<IEnumerable<User>> GetStatsAsync()
        {
            return Task.Run(() => GetStats());
        }
        public static IEnumerable<User> GetStats()
        {
            //TODO Indiquer route => liste user soit route soit signalR
            User tempo = new User();
            yield return tempo;
        }
        #endregion
    }
}
