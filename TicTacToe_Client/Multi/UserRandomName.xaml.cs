﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour UserRandomName.xaml
    /// </summary>
    public partial class UserRandomName : Page
    {
        public UserRandomName()
        {
            InitializeComponent();
        }

        private async void JoinLobby_Click(object sender, RoutedEventArgs e)
        {
            string id = P1TB.Text;
            string pass = P1Pass.Password;
            App.playerName = id;
            bool success  = await Multi.JoinLobbyAsync(id, pass);
            if (success)
                NavigationService.Navigate(new Uri(@"Multi/Accueil_Multi.xaml", UriKind.Relative));
            else
                //TODO display error
                MessageBox.Show("Le nom d'utilisateur ou le mot de passe est incorrect.");

        }

        private void CreateAcc_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://localhost:60584/login/signup");
        }

        private void Home_MouseDown(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Accueil.xaml", UriKind.Relative));
        }
    }
}
