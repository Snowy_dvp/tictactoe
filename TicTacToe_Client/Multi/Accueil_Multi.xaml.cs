﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.Generic;

namespace TicTacToe_DisizAndSnowy
{
    public partial class Accueil_Multi : Page
    {
        #region Public
        public static string localPlayerName;
        public static string distantPlayerName;

        public static string playerNameInviter;
        #endregion


        #region Private
        private bool connected = false;
        private HubConnection hubConnection;
        #endregion


        #region Constructeur
        public Accueil_Multi()
        {
            InitializeComponent();
            
            //-- Récupère nom joueur 1
            localPlayerName = App.playerName;
            App.mutliMode = true;
            WelcomeL.Content = "Bienvenue, " + localPlayerName;
            setupSignalR();
        }

        public async void setupSignalR()
        {
            hubConnection = new HubConnectionBuilder().WithUrl("http://localhost:60584/gameHub").Build();
            hubConnection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await hubConnection.StartAsync();
            };

            hubConnection.On<string, string>("Invite", (userNameInvited, userNameInviter) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (userNameInvited == localPlayerName)
                        invitationReceived(userNameInviter);
                });
            });

            hubConnection.On<string, string>("StartGame", (userName1, userName2) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (localPlayerName == userName1)
                        StartGame(userName2);
                    else if (localPlayerName == userName2)
                        StartGame(userName1);
                });
            });

            await hubConnection.StartAsync();
        }


        #endregion


        #region Chargement Page
        //-- Lancement partie 
        private void Home_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //-- Si invitation en cours deconnexion
            if (distantPlayerName != "")
                Multi.CancelInvit(distantPlayerName, localPlayerName);

            //-- On quitte le lobby
            Multi.LeaveLobby(localPlayerName);
            NavigationService.Navigate(new Uri(@"UserRandomName.xaml", UriKind.Relative));
        }


        //-- Stats multijoueur
        private void StatsB_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"ClassementPage.xaml", UriKind.Relative));
        }
        #endregion


        #region Invitations
        //-- Invitation recu
        public void invitationReceived(string distantUserName)
        {
            playerNameInviter = distantUserName;
            AcceptInvit.IsEnabled = true;
            RejectInvit.IsEnabled = true;
            InvitL.Content = playerNameInviter;
        }
        //-- Invitation acceptée
        private void AcceptInvit_Click(object sender, RoutedEventArgs e)
        {
            Multi.AcceptInvit(localPlayerName, playerNameInviter);

            
        }

        private void StartGame(string distantPlayerName)
        {
            Accueil_Multi.distantPlayerName = distantPlayerName;
            NavigationService.Navigate(new Uri(@"GamePage.xaml", UriKind.Relative));
        }


        private void RejectInvit_Click(object sender, RoutedEventArgs e)
        {
            Multi.DeclineInvitAsync(distantPlayerName, localPlayerName);
            InvitL.Content = "Aucune invitation en attente";
        }
        #endregion


        #region Liste User
        private async void Refresh_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            await RefreshUIAsync();
        }


        public async Task RefreshUIAsync()
        {
            ModelUserList[] listObj = await Multi.RefreshAsync();
            UpdateListBox(listObj);
        }


        private void UpdateListBox(ModelUserList[] listObj)
        {
            ClearListBox();
            InvitRun_L.Content = "";

            int index = 0;
            foreach (ModelUserList user in listObj)
            {
                if (user.Name != localPlayerName)
                {
                    ListJoueurs_LB.Items.Add(user.Name);
                    Button btn = new Button
                    {
                        Content = "Inviter",
                        Name = "Button" + index++.ToString(),
                    };
                    btn.FontSize = 12;
                    btn.Height = 19.5;
                    btn.Click += ListBoxClick;
                    ListButton.Children.Add(btn);
                }
            }
        }

        private void ClearListBox()
        {
            ListJoueurs_LB.Items.Clear();
            ListButton.Children.Clear();
        }


        protected void ListBoxClick(object sender, EventArgs e)
        {           
            string contentButton = ((Button)sender).Content.ToString();
            string nameButton = ((Button)sender).Name;
            string nameSplit = nameButton.Split('n')[1];
            int idSelected = int.Parse(nameSplit);
            distantPlayerName = (ListJoueurs_LB.Items[idSelected].ToString());

            if (!connected)
            {
                Multi.Invit(localPlayerName, distantPlayerName);

                InvitRun_L.Content = "Invitation en cours : " + distantPlayerName;
                //-- Boucle pour verouiller la liste des  boutons non invité
                foreach (object child in ListButton.Children)
                {
                    string nameChild = ((Button)child).Name;
                    if (nameChild != nameButton)
                        ((Button)child).IsEnabled = false;
                    else
                        ((Button)sender).Content = "Annuler";
                }                   
            }
            else
            {
                Multi.CancelInvit(distantPlayerName, localPlayerName);
                distantPlayerName = "";
                InvitRun_L.Content = "";
                foreach (object child in ListButton.Children)
                {
                    ((Button)child).IsEnabled = true;
                    ((Button)child).Content = "Inviter";
                }
            }
            connected = !connected; 
        }
        #endregion
    }
}
