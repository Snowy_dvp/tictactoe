﻿using System.IO;
using System.Windows;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructeur
        public MainWindow()
        {
            InitializeComponent();
            if (!File.Exists(Util_CSV.fileName + ".csv"))
                File.Create(Util_CSV.fileName + ".csv").Dispose();
        }
        #endregion
    }
}
