﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_DisizAndSnowy
{
    public class User
    {
        public string Name { get; set; }
        public double Win { get; set; }
        public double Lose { get; set; }
        public double Percentage { get; set; }

        public User()
        {}

        public User(string name, double win, double lose)
        {
            Name = name;
            Win = win;
            Lose = lose;
            Percentage = Math.Round(win / (win + lose) * 100, 2);
        }
    }
}
