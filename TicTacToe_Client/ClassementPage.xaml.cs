﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour ClassementPage.xaml
    /// </summary>
    public partial class ClassementPage : Page
    {
        #region Constructeur
        public ClassementPage()
        {
            InitializeComponent();
            if (!App.mutliMode)
            {
                TitreL.Visibility = Visibility.Visible;
                TitreL_Multi.Visibility = Visibility.Hidden;
                PrintStats();
            }
            else
            {
                //-- Méthode récupération stats multi
                TitreL.Visibility = Visibility.Hidden;
                TitreL_Multi.Visibility = Visibility.Visible;
                PrintStatsOnline();
            }
        }
        #endregion

        #region Interface Classement
        private void PrintStats()
        {
            ListViewPeople.ItemsSource = Util_CSV.ReadCSV();
        }

        private void PrintStatsOnline()
        {
            ListViewPeople.ItemsSource = Multi.GetStats();
        }


        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (!App.mutliMode) 
                NavigationService.Navigate(new Uri(@"Solo/Accueil_Solo.xaml", UriKind.Relative));
            else
                NavigationService.Navigate(new Uri(@"Multi/Accueil_Multi.xaml", UriKind.Relative));
        }


        private void Reinitialize_CSV_Click(object sender, RoutedEventArgs e)
        {
            Util_CSV.EmptyCSV();
            ListViewPeople.ItemsSource = null;
        }
        #endregion
    }
}
