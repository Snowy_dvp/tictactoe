﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace TicTacToe_DisizAndSnowy
{
    /// <summary>
    /// Logique d'interaction pour GamePage.xaml
    /// </summary>
    public partial class GamePage : Page, INotifyPropertyChanged
    {
        #region Private
        private GameView gameView;
        private bool gameOver = false;
        private bool yourTurn;
        #endregion

        #region Initialisation Partie
        public GamePage()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            if (App.mutliMode)
            {
                yourTurn = Multi.GameLaunch();
            }
            gameView = new GameView();
            ContainerGame.Children.Cast<Button>().ToList().ForEach(button =>
            {
                button.Content = string.Empty;
                button.Foreground = Brushes.Orange;
                button.IsEnabled = true;
            });
            DataContext = gameView;
        }
        #endregion


        #region Gestion Partie
        //-- Coup de J1
        private void Button_Click(object sender, System.Windows.RoutedEventArgs ev)
        {
            ClickEvent(sender);
        }

        //-- Coup de J2
        private void OpponentMove(int index)
        {
            //-- Conversion de l'index en ligne et colonne
            int col = index % 10;
            int row;
            if (index >= 10)
                row = index - col - 9;
            else
                row = 0; 

           //-- On récupère le bouton
           Button btnPlayed = (Button)Container.Children
                .Cast<UIElement>()
                .First(e => Grid.GetRow(e) == col && Grid.GetColumn(e) == row);

            //-- Coup joué
            ClickEvent(btnPlayed);;
        }


        private void ClickEvent(object sender)
        {
            //-- Evenement click
            if ((App.mutliMode && yourTurn) || (!App.mutliMode))
            {
                gameOver = gameView.OnClick(sender);

                //-- Si la partie est terminé
                if (gameOver)
                {
                    ContainerGame.Children.Cast<Button>().ToList().ForEach(button =>
                    {
                        button.IsEnabled = false;
                    });
                    Restart.IsEnabled = true;
                }
                yourTurn = !yourTurn;
            }
        }

        private void Restart_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Restart.IsEnabled = false;
            Init();
        }
        #endregion

        #region Retour
        private void Back_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!App.mutliMode)       
                NavigationService.Navigate(new Uri(@"Solo/Accueil_Solo.xaml", UriKind.Relative));         
            else
                NavigationService.Navigate(new Uri(@"Multi/Accueil_Multi.xaml", UriKind.Relative));
        }
        #endregion

        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
