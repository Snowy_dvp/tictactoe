﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Media;

namespace TicTacToe_DisizAndSnowy
{
    class GameView : INotifyPropertyChanged
    {
        #region Private
        //-- Actual cell states
        private CellType[,] nMark;

        private bool isP1Turn { get; set; }

        private bool isGameEnd;

        private int nbMove;

        private string playerTurn;

        private string player1;

        private string player2;
        #endregion

        #region Public
        public string Player1N { get; set; }

        public string Player2N { get; set; }

        public string PlayerTurn
        {
            get
            {
                return playerTurn;
            }
            set
            {
                playerTurn = value;
                OnPropertyChanged();   
            }
        }
        #endregion

        #region Initialisation Terrain
        public GameView()
        {
            InitGame();
        }


        public void InitGame()
        {
            //-- Tableau des cellules
            nMark = new CellType[3,3];
            for (int i = 0; i < nMark.GetLength(0); i++)
                for (int j = 0; j < nMark.GetLength(1); j++)
                    nMark[i,j] = CellType.Free;

            //-- Recevoir qui commence par serveur
            /*Random rand = new Random();
            isP1Turn = rand.Next(2) == 0 ? true : false;*/


            //-- Reinitialise fin partie
            isGameEnd = false;

            //-- Réinitialise le nombre de coups
            nbMove = 0;

            //-- On récupère le nom des joueurs
            if (!App.mutliMode)
            {
                player1 = Accueil_Solo.j1;
                player2 = Accueil_Solo.j2;
            }
            else
            {
                player1 = Accueil_Multi.localPlayerName;
                player2 = Accueil_Multi.distantPlayerName;
            }

            //-- Interface affichage
            Player1N = "Joueur 1 : \n" + player1;
            Player2N = "Joueur 2 : \n" + player2;
            if (isP1Turn)
                PlayerTurn = $"Tour de {player1}";
            else
                PlayerTurn = $"Tour de {player2}";
        }
        #endregion

        #region Gestion Partie
        public bool OnClick(object sender)
        {
            //-- On récupère le bouton
            var button = (Button)sender;

            //-- Coup joué
            nbMove++;

            //-- On récupère l'index colonne et ligne
            var col = Grid.GetColumn(button);
            var row = Grid.GetRow(button);

            //-- *3 ligne car 0/1/2 valeur de row
           // var index = col + (row * 3);

            //-- On retourne si la cellule n'est pas disponible
            if (nMark[col, row] != CellType.Free)
                return false;      

            //-- Contenu nMark
            nMark[col, row] = isP1Turn ? CellType.Cross : CellType.Circle;

            //-- Modification bouton
            button.Content = isP1Turn ? "X" : "O";
            button.IsEnabled = false;

            //-- Changer tour joueur
            isP1Turn = !isP1Turn;
            if (isP1Turn)
            {
                PlayerTurn = $"Tour de {player1}";
            }
            else
            {
                PlayerTurn = $"Tour de {player2}";
                button.Foreground = Brushes.OrangeRed;
            }         

            //-- On vérifie s'il y a un gagnant à chaque tour       
            CheckWin();

            //-- Si match nul
            if (nbMove >= 9)
            {
                EndGame(CellType.Free);
                isGameEnd = true;
            }
            if (isGameEnd)
                return true;
            else
                return false;
        }


        public void CheckWin()
        {
            #region Victoire Horizontale
            for (int i = 0; i < nMark.GetLength(0); i++)
            {
                if (nMark[i,0] != CellType.Free && nMark[i,0] == (nMark[i, 1] & nMark[i, 2]))
                {
                    isGameEnd = true;
                    EndGame(nMark[i, 0]);
                }
            }
            #endregion

            #region Victoire Verticale
            for (int j = 0; j < nMark.GetLength(0); j++)
            {
                if (nMark[0, j] != CellType.Free && nMark[0, j] == (nMark[1, j] & nMark[2, j]))
                {
                    isGameEnd = true;
                    EndGame(nMark[0, j]);
                }
            }
            #endregion

            #region Victoire Diagonale
            if (nMark[0, 0] != CellType.Free && (nMark[0, 0] & nMark[1, 1] & nMark[2, 2]) == nMark[0, 0])
            {
                isGameEnd = true;
                EndGame(nMark[0, 0]);
            }
            if (nMark[2, 0] != CellType.Free && (nMark[2, 0] & nMark[1, 1] & nMark[0, 2]) == nMark[2, 0])
            {
                isGameEnd = true;
                EndGame(nMark[2, 0]);
            }
            #endregion
        }
    

        public void EndGame(CellType cell)
        {
            //-- Egalité
            if (cell == CellType.Free)
            {
                PlayerTurn = "Match nul !!";
            }
            //-- Joueur 1
            if (cell == CellType.Cross)
            {
                PlayerTurn = $"Victoire pour {player1} !!";
                if (!App.mutliMode)
                {
                    Util_CSV.WriteCsv(player1, player2);
                }
                else
                {
                    Multi.SendStats();
                }


            }
            //-- Joueur 2
            if (cell == CellType.Circle)
            {
                PlayerTurn = $"Victoire pour {player2} !!";
                if (!App.mutliMode)
                {
                    Util_CSV.WriteCsv(player2, player1);
                }
                else
                {
                    Multi.SendStats();
                }

            }
        }
        #endregion

        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
