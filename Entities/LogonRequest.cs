﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class LogonRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
