﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe_Server.Model
{
    public class InvitationRequest
    {
        public string UserName1;
        public string UserName2;
    }
}
